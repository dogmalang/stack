"use strict";

var _core = require("@dogmalang/core");

const $Stack = class Stack {
  constructor(arg) {
    /* istanbul ignore next */
    _core.dogma.paramExpectedToBe("arg", arg, [_core.map, _core.list]);

    {
      if (_core.dogma.is(arg, _core.list)) {
        _core.dogma.update(_core.dogma.update(this, {
          name: "items",
          visib: ":",
          assign: "::=",
          value: _core.dogma.cloneL(arg)
        }), {
          name: "max",
          visib: ".",
          assign: "::=",
          value: null
        });
      } else if (_core.dogma.is(arg, _core.map)) {
        _core.dogma.update(_core.dogma.update(this, {
          name: "items",
          visib: ":",
          assign: "::=",
          value: []
        }), {
          name: ["max"],
          visib: ".",
          assign: "::=",
          value: arg,
          type: "mapped"
        });
      } else {
        _core.dogma.update(_core.dogma.update(this, {
          name: "items",
          visib: ":",
          assign: "::=",
          value: []
        }), {
          name: "max",
          visib: ".",
          assign: "::=",
          value: null
        });
      }
    }
  }

};
const Stack = new Proxy($Stack, {
  apply(receiver, self, args) {
    return new $Stack(...args);
  }

});
module.exports = exports = Stack;

Stack.prototype.append = Stack.prototype.push = function (item) {
  {
    if (this.max != null && (0, _core.len)(this._items) + 1 > this.max) {
      _core.dogma.raise("maximum number of items reached.");
    }

    this._items.push(item);
  }
  return this;
};

Stack.prototype.pop = function () {
  {
    if ((0, _core.len)(this._items) == 0) {
      _core.dogma.raise("stack is empty.");
    }

    return this._items.pop();
  }
};

Stack.prototype.len = function () {
  {
    return (0, _core.len)(this._items);
  }
};

Stack.prototype.isEmpty = function () {
  {
    return (0, _core.len)(this._items) == 0;
  }
};

Stack.prototype.top = function () {
  {
    if ((0, _core.len)(this._items) == 0) {
      _core.dogma.raise("stack is empty.");
    }

    return _core.dogma.getItem(this._items, -1);
  }
};

Stack.prototype.array = Stack.prototype.list = function () {
  {
    return _core.dogma.cloneL(this._items);
  }
};