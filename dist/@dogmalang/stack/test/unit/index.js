"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

const assert = _core.dogma.use(require("@ethronjs/assert"));

const pkg = _core.dogma.use(require("../../../../@dogmalang/stack"));

module.exports = exports = (0, _ethron.test)("assert()", () => {
  {
    assert(pkg).isCallable();
  }
});