"use strict";

var _core = require("@dogmalang/core");

var _ethron = require("ethron");

const assert = _core.dogma.use(require("@ethronjs/assert"));

const Stack = _core.dogma.use(require("../../../../@dogmalang/stack"));

module.exports = exports = (0, _ethron.suite)(__filename, () => {
  {
    (0, _ethron.suite)("constructor", () => {
      {
        (0, _ethron.test)("constructor()", () => {
          {
            const stack = Stack();
            assert(stack.max).isNil();
            assert(stack._items).eq([]);
          }
        });
        (0, _ethron.test)("constructor({max})", () => {
          {
            const stack = Stack({
              ["max"]: 123
            });
            assert(stack.max).eq(123);
            assert(stack._items).eq([]);
          }
        });
        (0, _ethron.test)("constructor(list)", () => {
          {
            const items = ["one", "two", "three"];
            const stack = Stack(items);
            assert(stack.max).isNil();
            assert(stack._items).eq(items).notSameAs(items);
          }
        });
      }
    });
    (0, _ethron.suite)("push()", () => {
      {
        (0, _ethron.test)("stack without max", () => {
          {
            const stack = Stack();
            stack.push("one").append("two");
            assert(stack.len()).eq(2);
            assert(stack._items).eq(["one", "two"]);
          }
        });
        (0, _ethron.test)("stack with max", () => {
          {
            const stack = Stack({
              ["max"]: 2
            });
            stack.push("one").push("two");
            assert(stack.len()).eq(2);
            assert(stack._items).eq(["one", "two"]);
            assert(() => {
              {
                stack.push("three");
              }
            }).raises("maximum number of items reached.");
            assert(stack.len()).eq(2);
            assert(stack._items).eq(["one", "two"]);
          }
        });
        (0, _ethron.test)("push(nil)", () => {
          {
            const stack = Stack().push(null);
            assert(stack._items).eq([null]);
          }
        });
      }
    });
    (0, _ethron.suite)("pop()", () => {
      {
        (0, _ethron.test)("with items", () => {
          {
            const stack = Stack().push("one").push("two");
            assert(stack.pop()).eq("two");
            assert(stack._items).eq(["one"]);
            assert(stack.pop()).eq("one");
            assert(stack._items).eq([]);
          }
        });
        (0, _ethron.test)("without items", () => {
          {
            assert(() => {
              {
                Stack().pop();
              }
            }).raises("stack is empty.");
          }
        });
      }
    });
    (0, _ethron.test)("len()", () => {
      {
        const stack = Stack();
        assert(stack.len()).eq(0);
        assert(stack.push("one").push("two").len()).eq(2);
      }
    });
    (0, _ethron.test)("isEmpty()", () => {
      {
        const stack = Stack();
        assert(stack.isEmpty()).eq(true);
        assert(stack.push("one").isEmpty()).eq(false);
      }
    });
    (0, _ethron.test)("top()", () => {
      {
        const stack = Stack();
        assert(() => {
          {
            stack.top();
          }
        }).raises("stack is empty.");
        assert(stack.push("one").top()).eq("one");
        assert(stack.push("two").top()).eq("two");
      }
    });
    (0, _ethron.test)("list()", () => {
      {
        const stack = Stack(["one", "two", "three"]);
        const arr = stack.list();
        assert(stack._items).eq(arr).notSameAs(arr);
      }
    });
  }
});