# @dogmalang/stack

[![NPM version](https://img.shields.io/npm/v/@dogmalang/stack.svg)](https://npmjs.org/package/@dogmalang/stack)
[![Total downloads](https://img.shields.io/npm/dt/@dogmalang/stack.svg)](https://npmjs.org/package/@dogmalang/stack)

A stack implementation.

*Developed in [Dogma](http://dogmalang.com), compiled to JavaScript.*

*Engineered in Valencia, Spain, EU by Justo Labs.*

## Use

The package must be imported as follows:

```
////////////////
// JavaScript //
////////////////
const Stack = require("@dogmalang/stack");

#########
# Dogma #
#########
use "@dogmalang/stack" as Stack
```

## Constructor

```
////////////////
// JavaScript //
////////////////
constructor()
constructor(items:any[])
constructor(opts:{max:number})

#########
# Dogma #
#########
type Stack()
type Stack(items:list)
type Stack(opts:{max:num})
```

- `items`, the initial items.
- `opts`, the options: `max`, maximum number of items.

Examples:

```
////////////////
// JavaScript //
////////////////
s = new Stack()
s = new Stack(["one", "two", "three"])
s = new Stack({max: 123})

#########
# Dogma #
#########
s = Stack()
s = Stack(["one", "two", "three"])
s = Stack({max=123})
```

## push() and append()

Add a new item to the stack:

```
////////////////
// JavaScript //
////////////////
push(item) : Stack

#########
# Dogma #
#########
fn Stack.push(item) -> self
```

The method returns the stack for chaining other pushes.
If the stack has `max` and this reached, an error is raised.

Example:

```
s.push("one").push("two").push("three")
```

## pop()

Remove the top item and returning it:

```
////////////////
// JavaScript //
////////////////
pop() : any

#########
# Dogma #
#########
fn Stack.pop() : any
```

If the stack is empty, an error is raised.

Example:

```
i = s.pop()
```

## len()

Return the current length:

```
////////////////
// JavaScript //
////////////////
len() : number

#########
# Dogma #
#########
fn Stack.len() : num
```

Example:

```
size = s.len()
```

## isEmpty()

Check whether the stack is empty:

```
////////////////
// JavaScript //
////////////////
isEmpty() : boolean

#########
# Dogma #
#########
fn Stack.isEmpty() : bool
```

Example:

```
s.isEmpty()
```

## top()

Return the top item without removing it:

```
////////////////
// JavaScript //
////////////////
top() : any

#########
# Dogma #
#########
fn Stack.top() : any
```

Example:

```
s.push("one").push("two").top()
```

## list()

Return a list with the items:

```
////////////////
// JavaScript //
////////////////
list() : any[]

#########
# Dogma #
#########
fn Stack.list() : list
```

Example:

```
s.list()
```
